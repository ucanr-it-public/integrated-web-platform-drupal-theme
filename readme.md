# UCANR Drupal Theme for Integrated Web Platform

## Getting started
Note: browsersync and file watching will not work unless you run this on your local machine but will require installing node/npm locally. To install nodejs and npm locally, visit [https://nodejs.org/](https://nodejs.org/)

1. Clone this repo to your themes directory (if it's not already there) 
2. Run `npm install` in the root of this theme
3. Add and initialize the `anr-bootstrap` repo inside the `scss` folder

        npm install
        git submodule update --init --recursive
        cd scss/anr-bootstrap-2021
        npm install
        cd ../..
        gulp