/* 
    Bootstrap Carousel enhancement to allow nesting and create a 'nested navigation slider'
*/

window.onload = function() {
    let transitionElements = document.querySelectorAll('.transition-w');
                
    //console.log(document.querySelector('#carouselTopicNavigation_topics'));
    let primaryCarousel = new bootstrap.Carousel(document.querySelector('#carouselViewnavigation_topics'));
    let primaryCarouselControls = document.querySelectorAll('#carouselViewnavigation_topics .carousel-control-ext');

    //console.log(transitionElements);

    transitionElements.forEach(transitionElement => {
        let transitionCol1 = transitionElement.querySelector('.transition-col-1');
        let transitionCol2 = transitionElement.querySelector('.transition-col-2');
        //console.log(transitionElement);

        transitionElement.addEventListener('transitionrun', function() {
            //if (transitionElement.classList.contains('active')) {
                transitionCol2.classList.remove('flex-shrink-1');
            //}
        });

        transitionElement.addEventListener('transitionend', () => {
            if (transitionElement.classList.contains('active')) {
                transitionCol2.classList.add('flex-shrink-1');
            }
        });
        
        transitionCol1.onclick = function() {
            let container = transitionElement.parentElement;
            //let carousel = new bootstrap.Carousel(transitionCol2.querySelector('.carousel'), {interval:false});
            //console.log(carousel);

            let elementPostion = Array.prototype.indexOf.call(container.children, transitionElement) + 1;
            
            let isFirst = transitionElement == container.firstElementChild;
            let isLast = !isFirst && (transitionElement == container.lastElementChild) && (elementPostion % 4 == 0);
            
            if (transitionElement.classList.contains('active')) {
                transitionElement.classList.remove('active');

                //document.querySelectorAll('.carousel-item-nested').forEach(ci => {
                //    ci.classList.remove('carousel-item');
                //});
            }
            else {
                transitionElements.forEach(te => {
                    te.classList.remove('active');
                });

                // since bootstrap doesn't support nested carousels, add .carousel-item class dynamically
                //document.querySelectorAll('.carousel-item-nested').forEach(ci => {
                //    ci.classList.add('carousel-item');
                //});

                if (isLast) {
                    container.classList.add('transition-container-reverse');
                }
                else if (isFirst) {
                    container.classList.remove('transition-container-reverse');
                }

                //carousel.to(0);

                this.style.width = this.clientWidth + 'px';
                transitionCol2.classList.remove('d-none');
                transitionElement.classList.add('active');
            }
        }
    });

    // override previous and next buttons to clear nested .carousel-item classes before sliding
    primaryCarouselControls.forEach(control => {
        control.onclick = function() {
            transitionElements.forEach(te => {
                te.classList.remove('active');
            });

            document.querySelectorAll('.carousel-item-nested').forEach(ci => {
                ci.classList.remove('carousel-item');
            });

            if (control.dataset.bsSlide == 'prev') {
                primaryCarousel.prev();
            }
            else {
                primaryCarousel.next();
            }
        }
    });
}