/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

  'use strict';

  Drupal.behaviors.iwp_theme = {
    attach: function(context, settings) {

      // make all carousel items the same height as the tallest item
      let minHeight = 0;
      $('.carousel.view .carousel-item').each(function() {
        minHeight = $(this).height() > minHeight ? $(this).height() : minHeight;
      });
      $('.carousel.view').not('.ignore-height').find('.carousel-item').css('min-height',minHeight);

      // Facets
      let facetsDropDown = '.js-facets-dropdown';
      $(facetsDropDown).addClass('form-select');

      $('.facets-soft-limit-link').click(function() {
        let linksList = $(this).parent().children('.js-facets-checkbox-links');
        if (linksList.hasClass('expanded')) {
          linksList.removeClass('expanded');
        }
        else {
          linksList.addClass('expanded');
        }
      });
      
    }
  };
})(jQuery, Drupal);