/*
    Boostrap submenu enhancement
*/

window.onload = function() {
    document.querySelectorAll('.submenu-toggle').forEach(function(element){
        let container = element.parentElement;
        let submenu = container.querySelector('.submenu');
        let interval;

        container.addEventListener('mouseover', function (e) {
            if (!submenu.classList.contains('show')) {
                submenu.classList.add('show');

                //if(interval) {              
                    //clearInterval(interval);
                //}
            }
        });

        container.addEventListener('mouseout', function (e) {
            if (submenu.classList.contains('show')) {
                //interval = setInterval(function() {
                    submenu.classList.remove('show');
                //}, 300);
            }
        });
    });
}